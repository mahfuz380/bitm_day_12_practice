<?php

$file = fopen("salaryinfo.txt", "r");
$fileLines = array();
$salaryArray = array();
while (!feof($file)) {
    $fileLines[] = fgets($file);
}
fclose($file);

$file = fopen("salaryinfo.txt", "r");
while (!feof($file)) {
    $salary = explode(" ",fgets($file));
    $salaryArray[]= $salary[1];
}
fclose($file);

$salaryAverage = array_sum($salaryArray)/count($salaryArray);
$salaryMaximum = max($salaryArray);
$salaryMinimum = min($salaryArray);

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $empId = isset($_POST['emp_id']) ? $_POST['emp_id'] : '';
    $empSalary = null;
    $empIdError = null;
    if($empId != ''){
        $file = fopen("salaryinfo.txt", "r");
        while (!feof($file)) {
            $salary = explode(" ",fgets($file));
            if ($salary[0] == $empId){
                $empSalary = $salary[1];
                $empIdError = null;
                break;
            }else{
                $empIdError = "Employee is not found";
            }
        }
        fclose($file);
    }
}



?>



<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Employer Salary</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col col-lg-6 align-self-center" style="margin-top: 75px;">
            <table class="table">
                <tbody>
                    <tr>
                        <td>Average Salary Amount : </td>
                        <td> <?php echo $salaryAverage; ?> </td>
                    </tr>
                    <tr>
                        <td>Maximum Salary Amount : </td>
                        <td> <?php echo $salaryMaximum; ?> </td>
                    </tr>
                    <tr>
                        <td>Minimum Salary Amount : </td>
                        <td> <?php echo $salaryMinimum; ?> </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col col-lg-6 align-self-center" style="margin-top: 75px;">
            <div class="user-form" >
                <form action="" method="post" >
                    <div class="form-group">
                        <label for="emp_id">Enter Employee ID </label>
                        <input type="text" name="emp_id" class="form-control" id="emp_id" >
                    </div>
                    <button type="submit" class="btn btn-primary">Show Salary</button>
                </form>
                <?php if(isset($empSalary)){  ?>
                    <h1>Salary Amount : <?php echo $empSalary; ?></h1>
                <?php } ?>
                <?php if(isset($empIdError)){  ?>
                    <h1><?php echo $empIdError; ?></h1>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>